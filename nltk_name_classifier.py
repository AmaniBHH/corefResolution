# nltk_name_classifier.py
# Python 2.7.6

"""
Classifier to determine the gender of a name using NLTK library

Classification - task of choosing the correct class label for a given input.

Supervised classifier:
Classifier that is built on training corpora containing the correct label
for each input.

Steps to create a classifier:
1) Decide what features of input are relevant and how to encode those features
    a. A feature extractor function is built to return a dictionary containing
     relevant information. The returned dictionary is known as feature set.
    b. Feature name is a case-sensitive string that provides human readable
     description of a feature (e.g. last_letter)
    c. Feature value are the extracted values of simple types, such as Boolean,
    numbers and strings.
2) Prepare a list of examples and corresponding class labels
3) Use the feature extractor to process the data and divide the resulting list
 of feature sets into a training set and test set
"""


import nltk
# Corpus which consists of male and female names dataset
from nltk.corpus import names
# For shuffling
import random


# Gender classifier
def gender_features(word):
    return {'last_letter': word[-1]}


