import tools
import nltk
# Corpus which consists of male and female names dataset
from nltk.corpus import names
# For shuffling
import random


# Gender classifier
def gender_features(word):
    return {'last_letter': word[-1]}

labeled_names = ([(name, 'male') for name in names.words('male.txt')] +
     [(name, 'female') for name in names.words('female.txt')])
random.shuffle(labeled_names)
featuresets = [(gender_features(n), gender) for (n, gender) in labeled_names]
train_set, test_set = featuresets[500:], featuresets[:500]
classifier = nltk.NaiveBayesClassifier.train(train_set)


test_sentence = "John ist ein Vater. Er hat ein Tochter. Sie ist Monika."
tokenized = tools.tokenize(test_sentence)
my_nlp = tools.my_nlp(test_sentence)

"""for token in my_nlp:
    print(token, "  ", token.i)"""

my_ne = tools.ner_paragraph(test_sentence)
print(my_ne)

""" extract pronouns from text : er and sie """
# er
pronouns_er = tools.find_pronoun(test_sentence, "er")
print(pronouns_er)
# sie
pronouns_sie = tools.find_pronoun(test_sentence, "sie")
print(pronouns_sie)

my_ne2 = []
for elem in my_ne:
    if elem[1] == 'PERSON':
        gen = classifier.classify(gender_features(elem[0].text))
        my_ne2.append([elem[0], elem[2], gen])

print(my_ne2)
min = 1000
coref = []
for element in my_ne2:
    if element[2] == 'male':
        if pronouns_er:
            for ind in pronouns_er:
                dist = abs(element[1] - ind)
                if dist < min:
                    dist = min
                    index = ind
                    name = element

    if element[2] == 'female':
        if pronouns_sie:
            for ind in pronouns_sie:
                dist = abs(element[1] - ind)
                if dist < min:
                    dist = min
                    index = ind
                    name = element

    coref.append([name, index])

print(coref)

