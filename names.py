import tools


sentence = "One, two, three, four, five" \
           "Everybody in the car, so come on let's ride" \
           "To the liquor store around the corner" \
           "The boys say they want some gin and juice" \
           "But I really don't wanna" \
           "Beer bust, like I had last week" \
           "I must stay deep, 'cause talk is cheap" \
           "I like Angela, Pamela, Sandra and Rita" \
           "And as I continue you know they're getting sweeter" \
           "So what can I do? I really beg you, my Lord" \
           "To me flirting is just like a sport" \
           "Anything fly, it's all good let me dump it" \
           "Please set in the trumpet" \
           "A little bit of Monica in my life" \
           "A little bit of Erica by my side" \
           "A little bit of Rita is all I need" \
           "A little bit of Tina is what I see" \
           "A little bit of Sandra in the sun" \
           "A little bit of Mary all night long" \
           "A little bit of Jessica here I am" \
           "A little bit of you makes me your man" \
           "Mambo No. 5!"

my_ners = tools.ner_sent_en(sentence)
print(my_ners)
